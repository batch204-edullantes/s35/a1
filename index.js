/*
	Mini-Activity:
		Create an expressJS designated to port 4000
		Create a new route with endpoint /hello and method GET
			-Should be able to response with "Hello World"
*/

const express = require("express");

const app = express();

// Mongoose is a package that allows creation of Schemas to model our data structures
// Also has access to a number of methods for manipulating our database
const mongoose = require("mongoose");

const port = 4000;

// MongoDB Connection
/*
	Syntax:
		mongoose.connect("<MongoDB Atlas Connection String>");
*/

mongoose.connect(
	'mongodb+srv://batch204:admin123@batch204.gsk1alv.mongodb.net/b204-to-dos?retryWrites=true&w=majority', 
	{
		useNewUrlParser:true, 
		useUnifiedTopology: true
	}
);

// Set notifications for connection success or failure
let db = mongoose.connection;

// If a connection error occured, output in the console
db.on("error", console.error.bind(console, "Connection Error"));

// If the connection is successful, output in the console
db.once("open", () => console.log("We're connected to the cloud database."));

// Mongoose Schema
// Schemas determine the structure of the documents to be written in the database
// Schemas acts as blueprints to our data

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
});


const UserSchema = new mongoose.Schema({
	username: String,
	password: String
});

// Models
// The first parameter of the Mongoose model method indicates the collection in where to store the data
// The second parameter is used to specify the schema/blueprint of the documents that will be stored in the MongoDB collection
const Task = mongoose.model("Task", taskSchema);

const User = mongoose.model("User", UserSchema);

app.use(express.json());

app.get("/hello", (req, res) => {
	res.send("Hello World");
});

// Route to create a Task 
app.post("/tasks", (req, res) => {
	Task.findOne({name: req.body.name}, (err, result) => {
		if(result != null && result.name == req.body.name) {
			return res.send("Duplicate Task Found");
		} else {
			let newTask = new Task({
				name: req.body.name
			});

			newTask.save((saveErr, savedTask) => {
				if(saveErr) {
					return console.error(saveErr);
				} else {
					return res.status(201).send("New Task created");
				}
			});
		}
	});
});

// Get all the tasks

app.get("/tasks", (req, res) => {
	Task.find({}, (err, result) => {
		if(err) {
			return console.log(err);
		} else {
			return res.status(200).json({
				data:result
			});
		}
	});
})

app.post("/signup", (req, res) => {
	User.findOne({username: req.body.username}, (err, result) => {
		if(result != null && result.username == req.body.username) {
			return res.send("User already exist!");
		} else {
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			});

			newUser.save((saveErr, savedUser) => {
				if(saveErr) {
					return console.error(saveErr);
				} else {
					return res.status(201).send("New User Created");
				}
			});
		}
	});
})

app.listen(port, () => {
	console.log(`Server is running at localhost:${port}`)
});